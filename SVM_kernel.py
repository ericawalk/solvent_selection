# Eric Walker
# September 21, 2018
import numpy
from sklearn.svm import SVC
import pybel
import sys
sys.path.insert(0, '/export/zimmerman/ericwalk/bin/CIRpy')
import cirpy
import ast
from csv import reader
from csv import writer

first_cut = 250#7978
second_cut = 1994
X = []
y = []

# first make the training data
file = open('20180705_replace_solvent_diel.csv')
lines = list(reader(file,delimiter=','))
lines = lines[0:300]
file.close()

print 'Getting initial fingerprints \n'
for i,line in enumerate(lines):
    try:
        fingerprint_i = pybel.readstring("smi", line[2])
        fingerprint_i = fingerprint_i.calcfp(fptype="MACCS")
        line[2] = fingerprint_i
        # now the catalyst fingerprint
    except:
        line[2] = False
    try:
        line[11] = cirpy.resolve(line[11], "smiles")
        print i
        #print line[11]
    except:
        line[11] = False
    try:
        cat_fingerprint_i = pybel.readstring("smi", line[11])
        print cat_fingerprint_i
        cat_fingerprint_i = cat_fingerprint_i.calcfp(fptype="MACCS")
        print cat_fingerprint_i
        line[11] = cat_fingerprint_i
    except:
        line[11] = False
    X.append([line[2], line[11]])
    y.append([line[12]])

def my_kernel(X, Y):
    tanimoto_matrix = numpy.zeros((len(X), len(Y)))
    for i_alpha, x_value in enumerate(X):
        for i_beta, y_value in enumerate(Y):
            product_distance = x_value[0] | y_value[0]
            catalyst_distance = x_value[1] | y_value[1]
            tanimoto_distance = product_distance * catalyst_distance
            tanimoto_matrix[i_alpha, i_beta] = tanimoto_distance
        print i_alpha
    return tanimoto_matrix

clf = SVC(kernel=my_kernel)
clf.fit(X[0:first_cut], y[0:first_cut])
