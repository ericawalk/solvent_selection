#Eric Walker
#June 7, 2018
# custom kernels
# k nearest neighbor and support vector machine
# iterates over tanimoto

from csv import reader
from csv import writer
from operator import itemgetter
from collections import Counter
import numpy
import sys
sys.path.insert(0, '/export/zimmerman/ericwalk/bin/CIRpy')
import cirpy
import pybel

file = open('20180705_replace_solvent_diel.csv')
lines_j = list(reader(file,delimiter=','))
file.close()
lines = lines_j

####################################################
# my functions


def fingerprint(smiles):
    pybel_mol = pybel.readstring("smi", smiles)
    MACCS_fp = pybel_mol.calcfp(fptype="MACCS").bits #MACCS has 166 keys (length)
    MACCS_fp_vec = numpy.zeros((166))
    for j in MACCS_fp:
        MACCS_fp_vec[j-1] = 1
    if line_j[10] and line_j[11] in (None, ""):
        cat_name = line_j[10]
    elif line_j[11] not in (None, ""):
        cat_name = line_j[11]
    MACCS_fp_vec = list(MACCS_fp_vec)
    return MACC_fp_vec

####################################################
# k-nearest neighbor

# knn function:
def eval_knn(k, lines, lines_j):
    correct = 0
    second_guess_correct = 0
    incorrect = 0
    node_file = open('claisen_k_' + str(k) + '_nodes.csv', 'w+')
    edge_file = open('claisen_k_' + str(k) + '_edges.csv', 'w+')
    node_writer = writer(node_file)
    edge_writer = writer(edge_file)
    node_writer.writerow(['Id','Original line number','Reaction','Product SMILES','Reactant','Product','Time (Reaction Details) [h]','Temperature (Reaction Details) [h]','Pressure (Reaction Details) [Torr]','pH-Value (Reaction Details)','Yield (numerical)','Reagent','Catalyst','Solvent (Reaction Details','References','Links to Reaxys','(A)protic','Solvent dielectric constant','Boiling point [C]','raw catalyst'])
    edge_writer.writerow(['Source','Target','Weight','Type'])
    for i,line in enumerate(lines[1:],1):
        print i
        catmatch = False
        list_of_tanimotos = []
        list_of_edges = []
        for j, line_j in enumerate(lines_j[1:],1):
            tanimoto = []
            try:
                okay_cat = line[11]
            except:
                okay_cat = False
            try:
                okay_cat_j = line_j[11]
            except:
                okay_cat_j = False
            if okay_cat == okay_cat_j and not i==j and not line[11] in (None,''):
                #print 'line[11]' + line[11]
                #print 'line_j[11]' + line_j[11]
                try:
                    if line[2]:
                        tanimoto = line[2] | line_j[2]
                        catmatch = True
                        list_of_tanimotos.append(tanimoto)
                        list_of_edges.append([i,j])
                except:
                    pass
        if catmatch:
            list_of_tanimotos, list_of_edges = (list(x) for x in zip(*sorted(zip(list_of_tanimotos, list_of_edges), key=itemgetter(0), reverse=True)))
        if len(list_of_edges) >= k:
            list_of_edges = list_of_edges[0:k]
        if catmatch:
            for xind,edges in enumerate(list_of_edges):
                edge_writer.writerow(['ReactionNum' + format((edges[0]), '05d'), 'ReactionNum' + format((edges[1]), '05d'), list_of_tanimotos[xind], 'Undirected'])
        #line[2] = line[-1] # The product name rather than the fingerprint was stored at the end of the line.
        #del line[-1]
        if len(list_of_edges) > 0 and catmatch:
            my_line = ['ReactionNum' + format((i), '05d')] + line
            node_writer.writerow(my_line[0:-1])
    node_file.close()
    edge_file.close()

# rotation of line which calls knn function
print 'Getting initial fingerprints \n'
for i,line in enumerate(lines):
    line.append(line[2])
    try:
        fingerprint_i = pybel.readstring("smi", line[2])
        fingerprint_i = fingerprint_i.calcfp(fptype="MACCS")
        line[2] = fingerprint_i
    except:
        line[2] = False

lines_j = lines

for k in [1,3,10]:
    #print 'k: ' + str(k)
    eval_knn(k, lines, lines_j)

