#Eric Walker
#June 6, 2018
#see SVM.py
import numpy
import heapq
import matplotlib.pylab as plt
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression

first_cut = 2737
second_cut = 685

X = numpy.loadtxt(open("X.csv", "rb"), delimiter=",")
X.shape
y_file = open("y.csv", "rb")
y = y_file.readlines()
y_file.close()
y = [cat.rstrip() for cat in y]
y = numpy.asarray(y)

clf = SVC()
print X.shape
clf.fit(X[0:first_cut], y[0:first_cut])

predictions = clf.predict(X[first_cut+1:])
matches = 0. # initiate variable
validations = y[first_cut+1:]
for i in range(len(predictions)): # automatically count number of correct predictions
    print 'i: ' + str(i)
    print 'predictions: ' + predictions[i]
    print 'validations: ' + validations[i]
    if predictions[i] == validations[i]:
        matches = matches + 1.
        print 'matches: ' + str(matches)


print '\n Percentage corect predictions SVC: ' + str(matches/second_cut*100) + '%\n'

####################################

lr = LogisticRegression()
lr.fit(X[0:first_cut], y[0:first_cut])

predictions = lr.predict(X[first_cut+1:])
matches = 0. # initiate variable
validations = y[first_cut+1:]
for i in range(len(predictions)): # automatically count number of correct predictions
    print 'i: ' + str(i)
    print 'predictions: ' + predictions[i]
    print 'validations: ' + validations[i]
    if predictions[i] == validations[i]:
        matches = matches + 1.
        print 'matches: ' + str(matches)


print '\n Percentage corect predictions logistic regression: ' + str(matches/second_cut*100) + '%\n'
lr_result = str(matches/second_cut*100)

#################################
clf = MLPClassifier(hidden_layer_sizes=(332, 332))
clf.fit(X[0:first_cut], y[0:first_cut])

matches = 0. # initiate variable
top_three = 0.
validations = y[first_cut+1:]

predictions = clf.predict(X[first_cut+1:])
print clf.classes_
proba = clf.predict_proba(X[first_cut+1:])
print clf.classes_
for iota,row in enumerate(X[first_cut+1:],start=first_cut+1):
    print iota
    print row
    if predictions[iota-(first_cut+1)] == validations[iota-(first_cut+1)]:
        matches = matches + 1.
    predict = clf.predict_proba(row.reshape(1, -1))[0]
    print clf.classes_
    print predict
    print 'sum of probabilities: ' + str(sum(predict))
    largest = heapq.nlargest(3, predict)
    print largest
    top_ind = numpy.argpartition(predict, -3)[-3:]
    print top_ind
    top_predicts = [clf.classes_[ay] for ay in top_ind]
    print top_predicts
    if y[iota] in top_predicts:
        top_three = top_three + 1

print proba.shape

print '\n Percentage corect predictions logistic regression: ' + lr_result + '%\n'

print '\n Percentage corect predictions artificial neural network 2 hidden layers: ' + str(matches/second_cut*100) + '%\n'
print '\n Percentage corect with top 3 predictions: ' + str(top_three/second_cut*100) + '%\n'
print '# of predictions: ' + str(len(predictions))
confusion = confusion_matrix(validations, predictions)
confusion_ind = list(reversed(numpy.argsort(numpy.sum(confusion, axis=1))))
print 'confusion indicies: ' + str(confusion_ind)
print clf.classes_[confusion_ind]
confusion = confusion[confusion_ind]
confusion = confusion[:,confusion_ind]
print 'confusion row sums: ' + str(numpy.sum(confusion, axis=1))
confusion = normalize(confusion, axis=1, norm='l1')
plt.imshow(confusion, cmap='hot', interpolation='nearest')
plt.colorbar()
plt.show()
plt.colorbar()
