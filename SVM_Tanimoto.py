#Eric Walker
#June 6, 2018
#see SVM.py
import numpy
import heapq
import matplotlib.pylab as plt
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression
from random import shuffle
from csv import reader
from csv import writer

#first_cut = 9722 #eighty_percent
X = numpy.loadtxt(open("X.csv", "rb"), delimiter=",")
X.shape
#X = X.tolist()
y_file = open("y.csv", "rb")
y = y_file.readlines()
y_file.close()
y = [cat.rstrip() for cat in y]
y = numpy.asarray(y)
first_cut = int(round(0.8*len(y)))

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = numpy.random.permutation(len(a))
    return a[p], b[p]

X, y = unison_shuffled_copies(X,y)
#combined = list(zip(X,y))
#shuffle(combined)
#X[:], y[:] = zip(*combined)

#print X
def my_kernel(XX, Y):
    print XX.shape
    print Y.shape
    XX = XX[:,166:]
    Y = Y[:,166:]
    norm_1 = (XX ** 2).sum(axis=1).reshape(XX.shape[0], 1)
    norm_2 = (Y ** 2).sum(axis=1).reshape(Y.shape[0], 1)
    prod = XX.dot(Y.T)
    return prod / (norm_1 + norm_2.T - prod) #pykernel https://github.com/gmum/pykernels/blob/master/pykernels/regular.py; http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.92.483&rep=rep1&type=pdf
    #return prod

def my_linear_kernel(XX, Y):
    XX = XX[:,166:]
    Y = Y[:,166:]
    prod = XX.dot(Y.T)
    return prod

def SVC_Tanimoto(X,y,first_cut):
    clf = SVC(kernel=my_kernel)
    clf.fit(X[0:first_cut], y[0:first_cut])

    matches = 0. # initiate variable
    #validations = y[first_cut+1:]
    validations = y[first_cut+1:]

    #predictions = clf.predict(X[first_cut+1:])
    predictions = clf.predict(X[first_cut+1:])
    for i in range(len(predictions)):
        if predictions[i] == validations[i]:
            matches = matches + 1.
    print 'Percentage corect predictions SVC Tanimoto kernel: ' + str(matches/len(predictions)*100) + '%'
    SVC_top_1 = matches/len(predictions)*100

#################################### training data

    matches = 0. # initiate variable
    #validations = y[first_cut+1:]

    #predictions = clf.predict(X[first_cut+1:])
    #proba = clf.predict_proba(X[first_cut+1:])

    validations = y[:first_cut+1]

    predictions = clf.predict(X[:first_cut+1])

    for kappa in range(len(predictions)):
        if predictions[kappa] == validations[kappa]:
            matches = matches + 1.

    print 'Percentage corect on training data: ' + str(matches/len(predictions)*100) + '%'
    print '# of predictions: ' + str(len(predictions))
    SVC_top_1_training_data = matches/len(predictions)*100

#################################### linear kernel training data

    clf = SVC(kernel=my_linear_kernel)
    clf.fit(X[0:first_cut], y[0:first_cut])

    matches = 0. # initiate variable
    #validations = y[first_cut+1:]
    validations = y[:first_cut+1]

    #predictions = clf.predict(X[first_cut+1:])
    predictions = clf.predict(X[:first_cut+1])
    for i in range(len(predictions)):
        if predictions[i] == validations[i]:
            matches = matches + 1.
    print 'linear kernel training data: ' + str(matches/len(predictions)*100) + '%'
    SVC_top_1_lin_training_data = matches/len(predictions)*100
    return [SVC_top_1, SVC_top_1_training_data, SVC_top_1_lin_training_data]

list_of_list_of_SVC_accuracies = []
list_of_list_of_SVC_training_accuracies = []
list_of_list_of_SVC_lin_training_accuracies = []
for i in range(10):
    list_of_SVC_accuracies = []
    list_of_SVC_training_accuracies = []
    list_of_SVC_lin_training_accuracies = []
    for j in range(5):
        [accuracy_SVC, accuracy_SVC_training, accuracy_SVC_lin_training] = SVC_Tanimoto(X,y,first_cut)
        list_of_SVC_accuracies.append(accuracy_SVC)
        list_of_SVC_training_accuracies.append(accuracy_SVC_training)
        list_of_SVC_lin_training_accuracies.append(accuracy_SVC_lin_training)
        X = numpy.concatenate((X[first_cut:],X[0:first_cut]))
        y = numpy.concatenate((y[first_cut:],y[0:first_cut]))
    list_of_list_of_SVC_accuracies.append(list_of_SVC_accuracies)
    list_of_list_of_SVC_training_accuracies.append(list_of_SVC_training_accuracies)
    list_of_list_of_SVC_lin_training_accuracies.append(list_of_SVC_lin_training_accuracies)
    X, y = unison_shuffled_copies(X,y)
    #combined = list(zip(X,y))
    #shuffle(combined)
    #X[:], y[:] = zip(*combined)

with open('SVC_Tanimoto.csv','wb') as f:
    writeit = writer(f)
    writeit.writerow(['SVC Tanimoto:'])
    writeit.writerows(list_of_list_of_SVC_accuracies)
    writeit.writerow(['SVC Tanimoto training data:'])
    writeit.writerows(list_of_list_of_SVC_training_accuracies)
    writeit.writerow(['SVC linear training data:'])
    writeit.writerows(list_of_list_of_SVC_lin_training_accuracies)
    f.close()

