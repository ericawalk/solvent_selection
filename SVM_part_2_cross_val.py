#Eric Walker
#June 6, 2018
#see SVM.py
import numpy
import heapq
import matplotlib.pylab as plt
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression
from random import shuffle
from csv import reader
from csv import writer

#first_cut = 9722 #eighty_percent
X = numpy.loadtxt(open("X.csv", "rb"), delimiter=",")
X.shape
#X = X.tolist()
y_file = open("y.csv", "rb")
y = y_file.readlines()
y_file.close()
y = [cat.rstrip() for cat in y]
y = numpy.asarray(y)
first_cut = int(round(0.8*len(y)))

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = numpy.random.permutation(len(a))
    return a[p], b[p]

X, y = unison_shuffled_copies(X,y)
#combined = list(zip(X,y))
#shuffle(combined)
#X[:], y[:] = zip(*combined)

#print X
def SVC_dNN(X,y,first_cut):
    clf = SVC()
    clf.fit(X[0:first_cut], y[0:first_cut])

    matches = 0. # initiate variable
    top_three = 0.
    validations = y[first_cut+1:]

    predictions = clf.predict(X[first_cut+1:])
    for i in range(len(predictions)):
        if predictions[i] == validations[i]:
            matches = matches + 1.
    print 'Percentage corect predictions SVC: ' + str(matches/len(predictions)*100) + '%'
    SVC_top_1 = matches/len(predictions)*100

####################################
    clf = MLPClassifier(hidden_layer_sizes=(332, 332))
    clf.fit(X[0:first_cut], y[0:first_cut])

    matches = 0. # initiate variable
    top_three = 0.
    validations = y[first_cut+1:]

    predictions = clf.predict(X[first_cut+1:])
    proba = clf.predict_proba(X[first_cut+1:])

    for kappa in range(len(predictions)):
        if predictions[kappa] == validations[kappa]:
            matches = matches + 1.
 
    for iota,row in enumerate(X[first_cut+1:],start=first_cut+1):
        predict = clf.predict_proba(row.reshape(1, -1))[0]
        #print clf.classes_
        #print predict
        #print 'sum of probabilities: ' + str(sum(predict))
        #largest = heapq.nlargest(3, predict)
        #print largest
        top_ind = numpy.argpartition(predict, -3)[-3:]
        #print top_ind
        top_predicts = [clf.classes_[ay] for ay in top_ind]
        #print top_predicts
        if y[iota] in top_predicts:
            top_three = top_three + 1


    print 'Percentage corect predictions artificial neural network 2 hidden layers: ' + str(matches/len(predictions)*100) + '%'
    print 'Percentage corect with top 3 predictions: ' + str(top_three/len(predictions)*100) + '%'
    print '# of predictions: ' + str(len(predictions))
    dNN_top_1 = matches/len(predictions)*100
    dNN_top_3 = top_three/len(predictions)*100
    return [SVC_top_1, dNN_top_1, dNN_top_3]

list_of_list_of_SVC_accuracies = []
list_of_list_of_dNN_accuracies = []
list_of_list_of_dNN_top_3_accuracies = []
for i in range(10):
    list_of_SVC_accuracies = []
    list_of_dNN_accuracies = []
    list_of_dNN_top_3_accuracies = []
    for j in range(5):
        [accuracy_SVC, accuracy_dNN, accuracy_dNN_top_3] = SVC_dNN(X,y,first_cut)
        list_of_SVC_accuracies.append(accuracy_SVC)
        list_of_dNN_accuracies.append(accuracy_dNN)
        list_of_dNN_top_3_accuracies.append(accuracy_dNN_top_3)
        X = numpy.concatenate((X[first_cut:],X[0:first_cut]))
        y = numpy.concatenate((y[first_cut:],y[0:first_cut]))
    list_of_list_of_SVC_accuracies.append(list_of_SVC_accuracies)
    list_of_list_of_dNN_accuracies.append(list_of_dNN_accuracies)
    list_of_list_of_dNN_top_3_accuracies.append(list_of_dNN_top_3_accuracies)
    X, y = unison_shuffled_copies(X,y)
    #combined = list(zip(X,y))
    #shuffle(combined)
    #X[:], y[:] = zip(*combined)

with open('dNN_out.csv','wb') as f:
    writeit = writer(f)
    writeit.writerow(['top 1:'])
    writeit.writerows(list_of_list_of_dNN_accuracies)
    writeit.writerow(['top 3:'])
    writeit.writerows(list_of_list_of_dNN_top_3_accuracies)
    f.close()

with open('SVC_out.csv','wb') as g:
    writeg = writer(g)
    writeg.writerow(['top 1:'])
    writeg.writerows(list_of_list_of_SVC_accuracies)
    g.close()
