#Eric Walker
#June 29 2018
#This script accomplishes two tasks in sorting the catalysts.  Once the catalysts are sorted, the script will be used in the delta function of catalyst matching for both k-nearest neighbor and a support vector machine with a custom kernel/Gram matrix.  Both are implemented in baseline (baseline_gamma.py or later) after successful demonstration of catalyst classification.
from csv import reader
from csv import writer
from collections import OrderedDict
from collections import Counter
import sys
sys.path.insert(0, '/export/zimmerman/ericwalk/bin/CIRpy')
import cirpy
import json
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text
od = OrderedDict([("Zinc", "Zn"), ("zinc", "Zn"), ("ytterbium", "Yb"), ("Ytterbium", "Yb"), ("P2W", "PW"), ("AlCl3, aluminium chloride", "Al trichloride"), ("Actinium", "Ac"),("Silver",	"Ag"), ("Aluminum", "Al"), ("Americium", "Am"), ("Argon", "Ar"), ("Arsenic", "As"), ("Astatine", "At"), ("Gold", "Au"), ("Boron", "B"), ("Barium", "Ba"), ("Beryllium",	"Be"), ("Bohrium", "Bh"), ("Bismuth", "Bi"), ("Berkelium", "Bk"), ("Bromine", "Br"), ("Calcium	", "Ca"), ("Cadmium", "Cd"), ("Cerium", "Ce"), ("Cobalt", "Co"), ("Chromium", "Cr"), ("Cesium",	"Cs"), ("Copper", "Cu"), ("Iron", "Fe"), ("Germanium", "Ge"), ("Mercury", "Hg"), ("Indium", "In"), ("Iridium", "Ir"), ("Potassium", "K"), ("Lanthanum",	"La"), ("Lithium", "Li"), ("Magnesium", "Mg"), ("Manganese", "Mn"), ("Molybdenum", "Mo"), ("Sodium", "Na"), ("Niobium", "Nb"), ("Nickel", "Ni"), ("Osmium", "Os"), ("Protactinium",	"Pa"), ("Lead", "Pb"), ("Palladium", "Pd"), ("Praseodymium", "Pr"), ("Platinum", "Pt"), ("Rubidium", "Rb"), ("Rhenium", "Re"), ("Rhodium", "Rh"), ("Ruthenium", "Ru"), ("Scandium", "Sc"), ("Selenium", "Se"), ("Silicon", "Si"), ("Tin", "Sn"), ("Strontium", "Sr"), ("Titanium", "Ti"), ("Vanadium", "V"), ("Tungsten", "W"), ("Zirconium",	"Zr"), ("actinium", "Ac"),("silver",       "Ag"), ("aluminum", "Al"), ("americium", "Am"), ("argon", "Ar"), ("arsenic", "As"), ("astatine", "At"), ("gold", "Au"), ("boron", "B"), ("barium", "Ba"), ("beryllium", "Be"), ("bohrium", "Bh"), ("bismuth", "Bi"), ("berkelium", "Bk"), ("bromine", "Br"), ("calcium  ", "Ca"), ("cadmium", "Cd"), ("cerium", "Ce"), ("cobalt", "Co"), ("chromium", "Cr"), ("cesium", "Cs"), ("copper", "Cu"), ("iron", "Fe"), ("germanium", "Ge"), ("mercury", "Hg"), ("indium", "In"), ("iridium", "Ir"), ("potassium", "K"), ("lanthanum", "La"), ("lithium", "Li"), ("magnesium", "Mg"),("manganese", "Mn"), ("molybdenum", "Mo"), ("sodium", "Na"), ("niobium", "Nb"), ("nickel", "Ni"), ("osmium", "Os"), ("protactinium", "Pa"), ("lead", "Pb"), ("palladium", "Pd"), ("praseodymium", "Pr"), ("platinum", "Pt"), ("rubidium", "Rb"), ("rhenium", "Re"), ("rhodium", "Rh"), ("ruthenium", "Ru"), ("scandium", "Sc"), ("selenium", "Se"), ("silicon", "Si"), ("tin", "Sn"), ("strontium", "Sr"), ("titanium", "Ti"), ("vanadium", "V"), ("tungsten", "W"), ("zirconium", "Zr"), ("Iodine", "I"), ("iodine", "I"), ("sodium hydroxide", "K hydroxide"), ("sodium ethanolate", "Na methanolate")])

file = open('20180503_replaced_solvent_diel.csv')
lines = list(reader(file))
transformed = open('20180705_replace_solvent_diel.csv', 'w+')
transformed_writer = writer(transformed)
#transformed_writer.writerow(['Catalyst'])
#open up the sorted catalysts list
# Use this block to make the split catalysts most frequent file
catlist = []
for line in lines: #for the first loop, the semicolons are split up#  
    if line[10] and line[11] in (None, ""):
        line[11] = line[10]
        line[10] = ''
    catalyst = line[11]
    catalyst = catalyst.split(';')
    for cat in catalyst:
        catlist.append(cat)
sorted_lines = [] #ascending list
sorted_intermediate = Counter(catlist).most_common()
for cat_intermediate in reversed(sorted_intermediate):
    sorted_lines.append(cat_intermediate[0])

########################################

dictionary = {}

def resolve_cirpy(name):
    global dictionary
    try:
        if name not in dictionary:
            temp_name = cirpy.resolve(name,'iupac_name')
            if type(temp_name) != str:
                temp_name = temp_name[-1]
            dictionary[name] = temp_name
        return dictionary[name]
    except:
        return name


def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text
od = OrderedDict([("hydoxide", "hydroxide"),("HYDOXIDE","hydroxide"),("Zinc", "Zn"), ("zinc", "Zn"), ("ytterbium", "Yb"), ("Ytterbium", "Yb"), ("P2W", "PW"), ("AlCl3, aluminium chloride", "Al trichloride"), ("Actinium", "Ac"),("Silver",       "Ag"), ("Aluminum", "Al"), ("Americium", "Am"), ("Argon", "Ar"), ("Arsenic", "As"), ("Astatine", "At"), ("Gold", "Au"), ("Boron", "B"), ("Barium", "Ba"), ("Beryllium", "Be"), ("Bohrium", "Bh"), ("Bismuth", "Bi"), ("Berkelium", "Bk"), ("Bromine", "Br"), ("Calcium  ", "Ca"), ("Cadmium", "Cd"), ("Cerium", "Ce"), ("Cobalt", "Co"), ("Chromium", "Cr"), ("Cesium", "Cs"), ("Copper", "Cu"), ("Iron", "Fe"), ("Germanium", "Ge"), ("Mercury", "Hg"), ("Indium", "In"), ("Iridium", "Ir"), ("Potassium", "K"), ("Lanthanum", "La"), ("Lithium", "Li"), ("Magnesium", "Mg"), ("Manganese", "Mn"), ("Molybdenum", "Mo"), ("Sodium", "Na"), ("Niobium", "Nb"), ("Nickel", "Ni"), ("Osmium", "Os"), ("Protactinium",     "Pa"), ("Lead", "Pb"), ("Palladium", "Pd"), ("Praseodymium", "Pr"), ("Platinum", "Pt"), ("Rubidium", "Rb"), ("Rhenium", "Re"), ("Rhodium", "Rh"), ("Ruthenium", "Ru"), ("Scandium", "Sc"), ("Selenium", "Se"), ("Silicon", "Si"), ("Tin", "Sn"), ("Strontium", "Sr"), ("Titanium", "Ti"), ("Vanadium", "V"), ("Tungsten", "W"), ("Zirconium",   "Zr"), ("actinium", "Ac"),("silver",       "Ag"), ("aluminum", "Al"), ("americium", "Am"), ("argon", "Ar"), ("arsenic", "As"), ("astatine", "At"), ("gold", "Au"), ("boron", "B"), ("barium", "Ba"), ("beryllium", "Be"), ("bohrium", "Bh"), ("bismuth", "Bi"), ("berkelium", "Bk"), ("bromine", "Br"), ("calcium  ", "Ca"), ("cadmium", "Cd"), ("cerium", "Ce"), ("cobalt", "Co"), ("chromium", "Cr"), ("cesium", "Cs"), ("copper", "Cu"), ("iron", "Fe"), ("germanium", "Ge"), ("mercury", "Hg"), ("indium", "In"), ("iridium", "Ir"), ("potassium", "K"), ("lanthanum", "La"), ("lithium", "Li"), ("magnesium", "Mg"),("manganese", "Mn"), ("molybdenum", "Mo"), ("sodium", "Na"), ("niobium", "Nb"), ("nickel", "Ni"), ("osmium", "Os"), ("protactinium", "Pa"), ("lead", "Pb"), ("palladium", "Pd"), ("praseodymium", "Pr"), ("platinum", "Pt"), ("rubidium", "Rb"), ("rhenium", "Re"), ("rhodium", "Rh"), ("ruthenium", "Ru"), ("scandium", "Sc"), ("selenium", "Se"), ("silicon", "Si"), ("tin", "Sn"), ("strontium", "Sr"), ("titanium", "Ti"), ("vanadium", "V"), ("tungsten", "W"), ("zirconium", "Zr"), ("Iodine", "I"), ("iodine", "I"), ("trifluoromethanesulfonate; Yb(+3) cation", "trifluoromethanesulfonate"), ("In(+3) cation; trifluoromethanesulfonate", "trifluoromethanesulfonate"), ("2-methylpropan-2-olate; Zr(+4) cation", "Zr(IV)"), ("silica supported perchloric acid", "silica-supported perchloric acid"), ("Zinc trifluoromethanesulfonate", "trifluoromethanesulfate"), ("sodium hydroxide", "Na hydroxide"), ("potassium hydroxide", "K hydroxide"), ("sodium ethanolate", "Na ethanolate"), ("POTASSIUM HYDROXIDE", "K hydroxide"), ("SODIUM HYDROXIDE", "Na hydroxide")])

metal_ion_od = OrderedDict([("Zr(+4) cation tetrachloride", "Zr(IV)"), ("Zr tetrapropoxide propanol", "Zr(IV)"), ("Zn(ClO4)2", "Zn(II)"), ("Zn dinitrate hexahydrate", "Zn(II)"),("Zn diiodide", "Zn(II)"), ("Zn dibromide", "Zn(II)"), ("trichloroYb", "Yb(III)"), ("tribromoYb", "Yb"), ("trichloroAu", "Au(III)"), ("tribromoAu", "Au(III)"), ("TfN[Al(i-Bu)2]2", "TfN"), ("TfN([Al(Me)Cl]2)", "TfN"), ("Ti(IV) isopropylate", "Ti(IV)"), ("Ti(IV) dichlorodiisopropylate", "Ti(IV)"), ("Cu(II) bromide", "Cu(II)"), ("Cu dinitrate", "Cu(II)"), ("Al trichloride", "Al(III)"), ("Al tribromide", "Al(III)"), ("tribromoYb", "Yb(III)"), ("Zr(N(Si(methyl)2{C6H4-2-methoxy})2)2Cl2", "Zr(IV)"), ("Zn(CH3COO)2*2H2O", "Zn(II)"), ("Pd(+2) cation diacetate", "Pd(II)"), ("Rh(+2) cation diacetate", "Rh(II)"), ("palladium", "Pd"), ("Nb(+5) cation pentachloride", "Nb(V)"), ("Mg diperchlorate", "Mg(II)"), ("Mg dichloride", "Mg(II)"), ("Mg dibromide", "Mg(II)"), ("Yb(OTf)3*xH2O", "Yb(III) trifluoromethanesulfonate hydrate"), ("sodium hydroxide", "Na hydroxide"), ("potassium hydroxide", "K hydroxide"), ("sodium ethanolate", "Na ethanolate"), ("POTASSIUM HYDROXIDE", "K hydroxide"), ("SODIUM HYDROXIDE", "Na hydroxide")])

########################################

transformed_writer.writerow(['Original line number','Reaction','Product SMILES','Reactant','Product','Time (Reaction Details) [h]','Temperature (Reaction Details) [h]','Pressure (Reaction Details) [Torr]','pH-Value (Reaction Details)','Yield (numerical)','Reagent','Catalyst','Solvent (Reaction Details','References','Links to Reaxys','(A)protic','Solvent dielectric constant','Boiling point [C]','raw catalyst'])

for ind, line in enumerate(lines): #now get the most frequent from an ascending sorted list
    raw_catalyst = '' #reset these from the previous loop
    catalyst_split = ''
    catalyst = ''
    catalyst_saved = ''
    if line[10] and line[11] in (None, ""):
        line[11] = line[10]
        line[10] = ''
    catalyst = line[11]
    raw_catalyst = catalyst #We will append the original raw catalyst entry to the end of 'line'.
    print 'This is the original catalyst: ' + catalyst
    catalyst_split = catalyst.split(';')
    for jnd, line_sort in enumerate(sorted_lines):
        if line_sort in catalyst_split: #The most frequent catalyst will be the last one to pass this conditional
            catalyst = line_sort.strip()
            catalyst_saved = catalyst # in case the following transformations don't work, at least we want to remove semicolons so let's save the split catalyst at this point.
    # Now, all metals are standardized by element symbol
    catalyst = catalyst.strip()
    print 'This is the catalyst before Cirpy resolve: ' + catalyst
    catalyst = resolve_cirpy(catalyst)
    catalyst = replace_all(catalyst, od)
    print 'This is the catalyst before metal ion typing: ' + catalyst
    catalyst = replace_all(catalyst, metal_ion_od)
    if catalyst.isupper():
        catalyst = catalyst.lower()
    print 'This is the catalyst before reassigning blanks to the raw entry: ' + catalyst
    if catalyst in (None, ""):
        catalyst = line[11]
    if ';' in catalyst: # We don't want multiple catalysts which are uniquely identified by a semicolon!
        catalyst = catalyst_saved
    line[11] = catalyst
    print 'This is the catalyst written to the file: ' + catalyst + '\n'
    line.append(raw_catalyst)
    transformed_writer.writerow(line)

file.close()
#catalysts.close()
transformed.close()

with open('iupac_name_dictionary.txt', 'w+') as file:
    file.write(json.dumps(dictionary))
