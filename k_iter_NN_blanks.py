#Eric Walker
#October 1, 2018
# predict on missing solvents

from csv import reader
from csv import writer
from operator import itemgetter
from collections import Counter
import numpy
import sys
sys.path.insert(0, '/export/zimmerman/ericwalk/bin/CIRpy')
import cirpy
import pybel
from random import shuffle

twenty_percent = 1320
file = open('20180705_replace_solvent_diel.csv')
lines_j = list(reader(file,delimiter=','))
file.close()
file_beta = open('20180705_replace_solvent_diel_blanks_short.csv')
lines = list(reader(file_beta,delimiter=','))

####################################################
# my functions

def my_kernel(X, Y):
    return numpy.dot(X, Y)

def fingerprint(smiles):
    pybel_mol = pybel.readstring("smi", smiles)
    MACCS_fp = pybel_mol.calcfp(fptype="MACCS").bits #MACCS has 166 keys (length)
    MACCS_fp_vec = numpy.zeros((166))
    for j in MACCS_fp:
        MACCS_fp_vec[j-1] = 1
    if line_j[10] and line_j[11] in (None, ""):
        cat_name = line_j[10]
    elif line_j[11] not in (None, ""):
        cat_name = line_j[11]
    MACCS_fp_vec = list(MACCS_fp_vec)
    return MACC_fp_vec

####################################################
# k-nearest neighbor

# knn function:
def eval_knn(k, twenty_percent, lines, lines_j):
    correct = 0
    second_guess_correct = 0
    third_guess_correct = 0
    incorrect = 0
    correct_tanimoto_means_sum = 0
    incorrect_tanimoto_means_sum = 0
    for i,line in enumerate(lines):
        catmatch = False
        list_of_solvents = []
        list_of_tanimotos = []
        if i>twenty_percent-1:
            break #stop at the first 20%
        #print 'Now checking for match for line number i: ' + str(i+1)
        predicted_solvent = False
        for j, line_j in enumerate(lines_j):
            tanimoto = []
            if line[11] == line_j[11]:# and not i==j:
                try:
                    tanimoto = line[2] | line_j[2]
                    predicted_solvent = line_j[12]
                    catmatch = True
                    list_of_solvents.append(predicted_solvent)
                    list_of_tanimotos.append(tanimoto)
                except:
                    pass
        if catmatch:
            list_of_tanimotos, list_of_solvents = (list(x) for x in zip(*sorted(zip(list_of_tanimotos, list_of_solvents), key=itemgetter(0), reverse=True)))
        #if i==1:
        #    print 'sample list of tanimotos (should be descending): ' + str(list_of_tanimotos)
        if len(list_of_solvents) >= k:
            list_of_solvents = list_of_solvents[0:k]
        if catmatch:
            los = Counter(list_of_solvents)
            predicted_solvent = los.most_common(1)[0][0]
        if predicted_solvent:
            print 'predicted solvent: ' + predicted_solvent
        try:
            there_are_two = los.most_common(2)[1][0]
        except:
            there_are_two = False
        if catmatch and there_are_two:
            second_predicted_solvent = there_are_two
            print 'second predicted solvent: ' + second_predicted_solvent
        else:
            second_predicted_solvent = False
        try:
            there_are_three = los.most_common(3)[2][0]
        except:
            there_are_three = False
        if catmatch and there_are_three:
            third_predicted_solvent = there_are_three
            print 'third predicted solvent: ' + third_predicted_solvent
        else:
            third_predicted_solvent = False
        if predicted_solvent:
            print 'catalyst: ' + line[11]
            print 'reaction: ' + line[1]
            print 'References: ' + line[13]
            print 'Link to Reaxys: ' + line[14]
            print '\n'
    return 1.

# rotation of line which calls knn function
print 'Getting initial fingerprints \n'
for i,line in enumerate(lines):
    try:
        fingerprint_i = pybel.readstring("smi", line[2])
        fingerprint_i = fingerprint_i.calcfp(fptype="MACCS")
        line[2] = fingerprint_i
    except:
        line[2] = False

for line_j in lines_j:
    try:
        fingerprint_j = pybel.readstring("smi", line_j[2])
        fingerprint_j = fingerprint_j.calcfp(fptype="MACCS")
        line_j[2] = fingerprint_j
    except:
        line_j[2] = False

shuffle(lines)
shuffle(lines_j)

k=10
percentage = eval_knn(k, twenty_percent, lines, lines_j)

