# Eric Walker December 13th, 2017
import pybel
import numpy as np
import csv
from csv import reader

file = open('claisen_condensation_1_12607_beta.csv')
lines = reader(file)
fileout = open('20171214_MACCS_fp_data.csv','w+')
outwriter = csv.writer(fileout) 

#Initialize variables
i = 0
lines_too_long = 0
num_exceptions = 0
num_many_empty = 0

for j,line in enumerate(lines):
    #print i
    #print 'Number of columns: ' + str(len(line))
    if j == 0:
        line.append('MACCS key')
        outwriter.writerow(line)
    try:
        Rxn_in_smiles = line[9]
        #print Rxn_in_smiles
        [reactant, product] = Rxn_in_smiles.split('>>')
        product = pybel.readstring("smi", product)
        product_fp = product.calcfp(fptype='MACCS').bits
        if i == 16589:
            print str(sum(len(x)==0 for x in line))
        if len(line) > 41:
            lines_too_long = lines_too_long + 1
            line = line[0:40]
        #if sum(len(x)==0 for x in line) > 20:
        #    num_many_empty = num_many_empty + 1
        if len(line[40]) == 0:
            num_many_empty = num_many_empty + 1
        if line[40] != '0' and len(line[14]) > 0:
            empty_vec = np.zeros((166))
            for j in product_fp:
                empty_vec[j-1] = 1
            line.extend(empty_vec.tolist())
            outwriter.writerow(line)
            i = i + 1
    except:
        num_exceptions = num_exceptions + 1 #Do nothing.
        
#MACCS_fp_data = open('20171214_MACCS_fp_data.csv','w+')
#MACCS_fp_data.writelines(write_lines)
#MACCS_fp_data.close()
file.close()
print 'Number of lines too long: ' + str(lines_too_long)
print 'Number of lines with empty number of reaction steps: ' + str(num_many_empty)
print 'Number of exceptions: ' + str(num_exceptions)
