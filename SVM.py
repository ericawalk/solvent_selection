# Eric Walker
# June 2, 2018
# This program runs a support vector classifier.
# The SVC is trained, then makes predictions.  
# The success rate of predictions is expressed as a percentage.
# Also, a deep neural net is included for comparison.
import numpy
from sklearn.svm import SVC
import pybel
import sys
sys.path.insert(0, '/export/zimmerman/ericwalk/bin/CIRpy')
import cirpy
import ast
from csv import reader
from csv import writer

X = []
y = []

# first make the training data
file = open('20180705_replace_solvent_diel.csv')
lines = list(reader(file,delimiter=','))
file.close()

X_file = open('X.csv', 'w+')
y_file = open('y.csv', 'w+')
diel_file = open('diel.csv', 'w+')
X_writer = writer(X_file)
y_writer = writer(y_file)
#diel_writer = writer(diel_file)

for i,line in enumerate(lines): #start=1320
    predicted_solvent = False
    print "now checking line: " + str(i)
    if line[10] and line[11] in (None, ""):
        line[11] = line[10]
        line[10] = ''
    catalyst = line[11]
    predicted_solvent = line[12] 
    if predicted_solvent:
        try:
            catalyst_smiles = cirpy.resolve(catalyst, 'smiles')
            print catalyst_smiles
            catalyst_mol = pybel.readstring("smi", catalyst_smiles)
            catalyst_fp = catalyst_mol.calcfp(fptype="MACCS").bits #MACCS has 166 keys (length)
            cat_fp_vec = numpy.zeros((166))
            for j in catalyst_fp:
                cat_fp_vec[j-1] = 1
            #print cat_fp_vec
        except:
            cat_fp_vec = numpy.zeros((166))
        try:
            product = line[2]
            print product
            product_mol = pybel.readstring("smi", product)
            product_fp = product_mol.calcfp(fptype="MACCS").bits #MACCS has 166 keys (length)
            pro_fp_vec = numpy.zeros((166))
            for j in product_fp:
                pro_fp_vec[j-1] = 1
            #print numpy.concatenate([cat_fp_vec,pro_fp_vec])
            #print [predicted_solvent.replace('\r\n','')]
        except:
            pro_fp_vec = numpy.zeros((166))
        X_writer.writerow(numpy.concatenate([cat_fp_vec,pro_fp_vec]))
        y_writer.writerow([predicted_solvent.replace('\r\n','')])
        #print float(line[-2])
        #diel_writer.writerow([line[-2]])

# after the data is filled in the arrays
X = numpy.asarray(X)
y = numpy.asarray(y)

# Use numpy.savetxt to write the text files to avoid cirpy again.
# train the model

X_file.close()
y_file.close()
diel_file.close()
#numpy.savetxt('X.txt',X)
#numpy.savetxt('y.txt',y)


