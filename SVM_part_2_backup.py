#Eric Walker
#June 6, 2018
#see SVM.py
import numpy
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier

X = numpy.loadtxt(open("X.csv", "rb"), delimiter=",")
X.shape
y_file = open("y.csv", "rb")
y = y_file.readlines()
y_file.close()
y = [cat.rstrip() for cat in y]
y = numpy.asarray(y)

clf = SVC()
print X.shape
clf.fit(X[0:7978], y[0:7978])

predictions = clf.predict(X[7979:])
matches = 0. # initiate variable
validations = y[7979:]
for i in range(len(predictions)): # automatically count number of correct predictions
    print 'i: ' + str(i)
    print 'predictions: ' + predictions[i]
    print 'validations: ' + validations[i]
    if predictions[i] == validations[i]:
        matches = matches + 1.
        print 'matches: ' + str(matches)


print '\n Percentage corect predictions SVC: ' + str(matches/1994*100) + '%\n'

clf = MLPClassifier(hidden_layer_sizes=(332, 332))
clf.fit(X[0:7978], y[0:7978])

predictions = clf.predict(X[7979:])
matches = 0. # initiate variable
validations = y[7979:]
for i in range(len(predictions)): # automatically count number of correct predictions
    print 'i: ' + str(i)
    print 'predictions: ' + predictions[i]
    print 'validations: ' + validations[i]
    if predictions[i] == validations[i]:
        matches = matches + 1.
        print 'matches: ' + str(matches)


print '\n Percentage corect predictions artificial neural network 2 hidden layers: ' + str(matches/1994*100) + '%\n'
