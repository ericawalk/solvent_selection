#Eric Walker
#June 6, 2018
#see SVM.py
import numpy
from sklearn.svm import SVC
from sklearn import metrics, svm
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import matplotlib

X = numpy.loadtxt(open("X.csv", "rb"), delimiter=",")
print X.shape
diel = numpy.loadtxt(open("diel.csv", "rb"), delimiter=",")
print diel.shape
base_pred_diel = numpy.loadtxt(open("base_pred_diel.csv", "rb"), delimiter=",")
plt.scatter(base_pred_diel[:,0], base_pred_diel[:,1], c="b", label='network match')
plt.xlabel('predicted solvent dielectric constant')
plt.ylabel('experimental solvent dielectric constant')
#diel_file = open("diel.csv", "rb")
#diel = diel_file.readlines()
#diel_file.close()
#diel = numpy.asarray(diel)

clf = svm.SVR()
clf.fit(X[0:7978], diel[0:7978])

predictions = clf.predict(X[7979:])
matches = 0. # initiate variable
validations = diel[7979:]
sum_sq_diel = 0.
sum_sq_diel_tot = 0.
for i in range(len(predictions)): # automatically count number of correct predictions
    print 'i: ' + str(i)
    print 'predictions: ' + str(predictions[i])
    print 'validations: ' + str(validations[i])
    sum_sq_diel = sum_sq_diel + (predictions[i] - validations[i])**2
    sum_sq_diel_tot = sum_sq_diel_tot + 1.
    print 'sum_sq_diel: ' + str(sum_sq_diel)
    print 'sum_sq_diel_tot: ' + str(sum_sq_diel_tot)

plt.scatter(predictions, validations, c="g", label="support vector regression")
print '\n standard deviation of prediction error SVM: ' + str((sum_sq_diel/sum_sq_diel_tot)**0.5) + '\n'

clf = MLPRegressor(hidden_layer_sizes=(332, 332))
clf.fit(X[0:7978], diel[0:7978])

predictions = clf.predict(X[7979:])
matches = 0. # initiate variable
validations = diel[7979:]
sum_sq_diel = 0.
sum_sq_diel_tot = 0.
for i in range(len(predictions)): # automatically count number of correct predictions
    print 'i: ' + str(i)
    print 'predictions: ' + str(predictions[i])
    print 'validations: ' + str(validations[i])
    sum_sq_diel = sum_sq_diel + (predictions[i] - validations[i])**2
    sum_sq_diel_tot = sum_sq_diel_tot + 1.
    print 'sum_sq_diel: ' + str(sum_sq_diel)
    print 'sum_sq_diel_tot: ' + str(sum_sq_diel_tot)

plt.scatter(predictions, validations, c="r", label="deep neural network")
print '\n standard deviation of prediction error artificial neural network 2 hidden layers: ' + str((sum_sq_diel/sum_sq_diel_tot)**0.5) + '\n'
plt.xlim(0, 45)
plt.ylim(0, 45)
plt.plot([0, 45],[0, 45], 'k-')
plt.legend(loc=2)
plt.show()
