#Eric Walker
# August 27, 2018
# network analysis with catalyst fingerprint to check for improved accuracy.

from csv import reader
from csv import writer
from operator import itemgetter
from collections import Counter
import numpy
import sys
sys.path.insert(0, '/export/zimmerman/ericwalk/bin/CIRpy')
import cirpy
import pybel
from random import shuffle

twenty_percent = 2431
file = open('20180705_replace_solvent_diel.csv')
lines_j = list(reader(file,delimiter=','))
file.close()
lines = lines_j

####################################################
# my functions

def my_kernel(X, Y):
    return numpy.dot(X, Y)

####################################################
# k-nearest neighbor

# knn function:
def eval_knn(k, twenty_percent, lines, lines_j):
    correct = 0
    second_guess_correct = 0
    third_guess_correct = 0
    incorrect = 0
    for i,line in enumerate(lines):
        catmatch = False
        list_of_solvents = []
        list_of_tanimotos = []
        if i>twenty_percent-1:
            break #stop at the first 20%
        #print 'Now checking for match for line number i: ' + str(i+1)
        predicted_solvent = False
        for j, line_j in enumerate(lines_j[twenty_percent:], twenty_percent):
            tanimoto = []
            if not i==j:
                try:
                    prod_tanimoto = line[2] | line_j[2]
                    cat_tanimoto = line[11] | line_j[11]
                    tanimoto = prod_tanimoto*cat_tanimoto
                    catmatch = True
                    predicted_solvent = line_j[12]
                    list_of_solvents.append(predicted_solvent)
                    list_of_tanimotos.append(tanimoto)
                except:
                    pass
        if catmatch:
            list_of_tanimotos, list_of_solvents = (list(x) for x in zip(*sorted(zip(list_of_tanimotos, list_of_solvents), key=itemgetter(0), reverse=True)))
        #if i==1:
        #    print 'sample list of tanimotos (should be descending): ' + str(list_of_tanimotos)
        if len(list_of_solvents) >= k:
            list_of_solvents = list_of_solvents[0:k]
        if catmatch:
            solvent_list_beta = []
            for solvent_alpha in list_of_solvents:
                if solvent_alpha not in solvent_list_beta:
                    solvent_list_beta.append(solvent_alpha)
            weights_total = numpy.zeros(len(solvent_list_beta))
            for i_beta, solvent_beta in enumerate(list_of_solvents):
                i_gamma = solvent_list_beta.index(solvent_beta)
                weights_total[i_gamma] = weights_total[i_gamma] + list_of_tanimotos[i_beta]
            i_delta = numpy.argsort(weights_total)
            predicted_solvent = solvent_list_beta[i_delta[-1]]
        try:
            there_are_two = solvent_list_beta[i_delta[-2]]
        except:
            there_are_two = False
        if catmatch and there_are_two:
            second_predicted_solvent = solvent_list_beta[i_delta[-2]]
        else:
            second_predicted_solvent = False
        try:
            there_are_three = solvent_list_beta[i_delta[-3]]
        except:
            there_are_three = False
        if catmatch and there_are_three:
            third_predicted_solvent = solvent_list_beta[i_delta[-3]]
        else:
            third_predicted_solvent = False
        #if i>1 and i<50 and there_are_two:
        #    print 'most frequent solvent among neighbors: ' + predicted_solvent
        #    print 'second most frequent: ' + second_predicted_solvent
        #    print 'actual solvent: ' + line[12]
        if line[12] == predicted_solvent:
            correct = correct + 1.
        elif line[12] == second_predicted_solvent:
            second_guess_correct = second_guess_correct + 1
        elif line[12] == third_predicted_solvent:
            third_guess_correct = third_guess_correct + 1
        elif not line[12] == predicted_solvent and catmatch:
            incorrect = incorrect + 1.
        else:
            pass
    print 'correct: ' + str(correct)
    print 'incorrect: ' + str(incorrect)
    percentage = correct/(correct + second_guess_correct + third_guess_correct + incorrect)*100.
    print 'knn prediction success rate: ' + str(percentage) + '%'
    second_percentage = (correct + second_guess_correct)/(correct + second_guess_correct + third_guess_correct + incorrect)*100.
    print 'knn top 2 prediction success rate: ' + str(second_percentage) + '%'
    third_percentage = (correct + second_guess_correct + third_guess_correct)/(correct + second_guess_correct + third_guess_correct + incorrect)*100
    print 'knn top 3 prediction success rate: ' + str(third_percentage) + '%'
    print 'fraction of data predicted on first prediction: ' + str((correct+incorrect)/twenty_percent) + '\n'
    return [percentage, second_percentage, third_percentage]

# rotation of line which calls knn function
print 'Getting initial fingerprints \n'
for i,line in enumerate(lines):
    try:
        fingerprint_i = pybel.readstring("smi", line[2])
        fingerprint_i = fingerprint_i.calcfp(fptype="MACCS")
        line[2] = fingerprint_i
        # now the catalyst fingerprint
    except:
        line[2] = False
    try:
        line[11] = cirpy.resolve(line[11], "smiles")
        print i
        #print line[11]
    except:
        line[11] = False
    try:
        cat_fingerprint_i = pybel.readstring("smi", line[11])
        print cat_fingerprint_i
        cat_fingerprint_i = cat_fingerprint_i.calcfp(fptype="MACCS")
        print cat_fingerprint_i
        line[11] = cat_fingerprint_i
    except:
        line[11] = False

with open('20180828_stored_fingerprints.csv', 'wb') as outfile:
    writeout = writer(outfile)
    writeout.writerows(lines)
    outfile.close()

shuffle(lines)
lines_j = lines

k=10
list_of_list_of_percentages = []
second_list_of_list_of_percentages = []
third_list_of_list_of_percentages = []
for zeta in range(10):
    list_of_percentages = []
    second_list_of_percentages = []
    third_list_of_percentages = []
    for yind in range(5):
        print 'k: ' + str(k)
        print 'data range (0:0-20%, 1:20-40%, 2:40-60%, 3:60-80%, 4:80-100%): ' + str(yind)
        percentage = eval_knn(k, twenty_percent, lines, lines_j)
        list_of_percentages.append(percentage[0])
        second_list_of_percentages.append(percentage[1])
        third_list_of_percentages.append(percentage[2])
        lines = lines[twenty_percent:] + lines[0:twenty_percent]
        lines_j = lines
    list_of_list_of_percentages.append(list_of_percentages)
    second_list_of_list_of_percentages.append(second_list_of_percentages)
    third_list_of_list_of_percentages.append(third_list_of_percentages)
    shuffle(lines)
    lines_j = lines
    
print 'final table: ' + str(list_of_list_of_percentages)
print 'final table top 2: ' + str(second_list_of_list_of_percentages)
print 'final table top 3: ' + str(third_list_of_list_of_percentages)

with open('knn_out_k_' + str(k) + '_top_cat_finger_weighted.csv', 'wb') as f:
    writeit = writer(f)
    writeit.writerow(['top 1:'])
    writeit.writerows(list_of_list_of_percentages)
    writeit.writerow(['top 2:'])
    writeit.writerows(second_list_of_list_of_percentages)
    writeit.writerow(['top 3:'])
    writeit.writerows(third_list_of_list_of_percentages)
    f.close()

#############################################
# SVM

#X = []
#y = []
#cat_names = []
#for line_j in lines_j:
#    try:
#        MACCS_fp_vec = fingerprint(line_j[2])
#        X.append(MACCS_fp_vec)
#        y.append(line_j[12])
#        if line_j[11]:
#            cat_name = line_j[11]
#        elif not line_j[11]:
#            cat_name = "empty"
#        cat_names.append(cat_name)
#    except:
#        print 'could not resolve product'
