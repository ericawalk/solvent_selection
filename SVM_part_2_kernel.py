#Eric Walker
#September 21, 2018
#see SVM.py
import numpy
import heapq
import matplotlib.pylab as plt
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression

X = numpy.loadtxt(open("X.csv", "rb"), delimiter=",")
X.shape
y_file = open("y.csv", "rb")
y = y_file.readlines()
y_file.close()
y = [cat.rstrip() for cat in y]
y = numpy.asarray(y)
first_cut = int(round(0.8*len(y)))

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = numpy.random.permutation(len(a))
    return a[p], b[p]

X, y = unison_shuffled_copies(X,y)

def my_kernel(XX, Y):
    print XX.shape
    print Y.shape
    norm_1 = (XX ** 2).sum(axis=1).reshape(XX.shape[0], 1)
    norm_2 = (Y ** 2).sum(axis=1).reshape(Y.shape[0], 1)
    prod = XX.dot(Y.T)
    #return prod / (norm_1 + norm_2.T - prod)
    return prod

clf = SVC(kernel=my_kernel)
#print X.shape
clf.fit(X[0:first_cut], y[0:first_cut])

predictions = clf.predict(X[first_cut+1:])
matches = 0. # initiate variable
validations = y[first_cut+1:]
for i in range(len(predictions)): # automatically count number of correct predictions
    #print 'i: ' + str(i)
    #print 'predictions: ' + predictions[i]
    #print 'validations: ' + validations[i]
    if predictions[i] == validations[i]:
        matches = matches + 1.
        #print 'matches: ' + str(matches)


print str(matches/len(predictions)*100)
